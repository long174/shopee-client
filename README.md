SHOPEE OPEN PLATFORM Client
===========

# Environment

## Must be declared environment variables:



SHOPEE_BASE_URL

SHOPEE_PARTNER

SHOPEE_SECRET


## How to use:
List Service:
```golang
GetToken(redirectURL string, partnerKey string) (result string)
GetOrders(ctx context.Context, shopperId int, request OrdersRequestDTO) (resp OrdersResponseDTO, err error)
GetOrderDetails(ctx context.Context, shopperId int, request OrderItemsRequestDTO) (resp OrderItemsResponseDTO, err error)
```

### Example:
```golang
c := shopee_client.NewService()
orderRequest := shopee_client.OrdersRequestDTO{
    OrderStatus: "ALL",
}
result, err := c.GetOrders(context.Background(), 213505855, orderRequest)

if err != nil {
    t.Error("expected nil but got: ", err)
}

fmt.Println(result)
```