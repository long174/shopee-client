package shopee_client

type ResponseDTO interface {
	GetMessage() string
}

type ResponseDTOBase struct {
	Msg string
}

func (s ResponseDTOBase) GetMessage() string {
	return s.Msg
}
