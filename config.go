package shopee_client

import (
	"github.com/kelseyhightower/envconfig"
)

type ShopeeConfig struct {
	ShopeeBaseUrl string `envconfig:"SHOPEE_BASE_URL"`
	ShopeePartner string `envconfig:"SHOPEE_PARTNER"`
	ShopeeSecret  string `envconfig:"SHOPEE_SECRET"`
}

type PlatformConfig struct {
	ShopeeBaseUrl   string
	ShopeePartnerId int
	ShopeeSecret    string
}

func (conf ShopeeConfig) GetConfigOfPlatform() *PlatformConfig {
	platforms := PlatformConfig{
		ShopeeBaseUrl:   "https://partner.shopeemobile.com",
		ShopeePartnerId: 845096,
		ShopeeSecret:    "141ab9257acdb158361efeca597389648d308daac7b49c5e28c0ce2ca310ffa3",
	}
	return &platforms
}

func GetAppConfigFromEnv() ShopeeConfig {
	var conf ShopeeConfig
	envconfig.MustProcess("", &conf)
	return conf
}
