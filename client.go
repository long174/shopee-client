package shopee_client

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type client struct {
	config     ShopeeConfig
	transport  http.RoundTripper
	httpClient *http.Client
}

func (c *client) DoRequest(
	ctx context.Context,
	shopperId int,
	method string,
	apiPath string,
	request interface{},
	result interface{},
) error {
	var (
		req *http.Request
		res *http.Response
		err error
	)

	req, err = c.buildRequest(shopperId, method, apiPath, request)
	if err != nil {
		return err
	}

	res, err = c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	err = c.checkResponseError(res)
	if err != nil {
		return err
	}

	if result == nil {
		return nil
	}

	//if res.ContentLength > 0 {
	err = json.NewDecoder(res.Body).Decode(result)

	if err != nil {
		return err
	}

	return nil
}

func (c *client) buildRequest(
	shopperId int,
	method string,
	apiPath string,
	request interface{},
) (req *http.Request, err error) {

	platformConfig := c.config.GetConfigOfPlatform()

	var params map[string]interface{}
	params, err = c.structToMap(request)
	if err != nil {
		return nil, err
	}

	timestamp := time.Now().Unix()
	params = c.mergeMaps(map[string]interface{}{
		"partner_id": platformConfig.ShopeePartnerId,
		"shopid":     shopperId,
		"timestamp":  timestamp,
	}, params)

	requestUrl := platformConfig.ShopeeBaseUrl + apiPath
	var buf *bytes.Buffer
	buf = bytes.NewBuffer([]byte{})
	err = json.NewEncoder(buf).Encode(params)
	if err != nil {
		return nil, err
	}
	req, err = http.NewRequest(method, requestUrl, buf)

	if err != nil {
		return nil, err
	}

	token, err := c.generateToken(requestUrl, params, platformConfig.ShopeeSecret)
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", "application/json")
	req.Header.Set("authorization", token)
	return req, nil
}

func (c *client) checkResponseError(res *http.Response) error {
	if res.StatusCode >= http.StatusBadRequest {
		err := &Error{
			Code: res.StatusCode,
		}
		if res.ContentLength > 0 {
			errDetail, _ := ioutil.ReadAll(res.Body)
			err.Message = string(errDetail)
		}
		return err
	}

	return nil
}

func (c *client) mergeMaps(maps ...map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

// Converts a struct to a map while maintaining the json alias as keys
func (c *client) structToMap(obj interface{}) (newMap map[string]interface{}, err error) {
	data, err := json.Marshal(obj) // Convert to a json string

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &newMap) // Convert to a map
	return
}

func (c *client) generateToken(baseURL string, params map[string]interface{}, secret string) (result string, err error) {
	sParam, err := json.Marshal(params)
	if err != nil {
		return "", err
	}

	data := baseURL + "|" + string(sParam) + "\n"
	h := hmac.New(sha256.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	return hex.EncodeToString(h.Sum(nil)), nil
}
