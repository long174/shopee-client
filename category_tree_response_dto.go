package shopee_client

type CategoryResponseDTO struct {
	Data      []CategoryNodeDTO `json:"data"`
	Code      string            `json:"code"`
	RequestId string            `json:"request_id"`
}

type CategoryNodeDTO struct {
	Children   []CategoryNodeDTO `json:"children"`
	Var        bool              `json:"var"`
	Name       string            `json:"name"`
	Leaf       bool              `json:"leaf"`
	CategoryId int               `json:"category_id"`
}
