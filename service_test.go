package shopee_client_test

import (
	shopee_client "bitbucket.org/snapmartinc/lzd-client"
	"context"
	"fmt"
	"testing"
)



func TestGetOrders(t *testing.T) {

	c := shopee_client.NewService()
	orderRequest := shopee_client.OrdersRequestDTO{
		OrderStatus: "ALL",
	}
	result, err := c.GetOrders(context.Background(), 213505855, orderRequest)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}


func TestGetToken(t *testing.T) {

	c := shopee_client.NewService()

	result := c.GetToken("https://www.efulfil.shop", "141ab9257acdb158361efeca597389648d308daac7b49c5e28c0ce2ca310ffa3", )

	fmt.Println(result)
}


func TestGetDetail(t *testing.T) {
	c := shopee_client.NewService()
	orderRequest := shopee_client.OrderItemsRequestDTO{
		OrdersnList: []string{"200301NWEWWB5W"},
	}
	result, err := c.GetOrderDetails(context.Background(), 213505855, orderRequest)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}
