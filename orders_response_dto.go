package shopee_client

type OrdersResponseDTO struct {
	Orders    []OrderResponseDTO `json:"orders"`
	More      bool               `json:"more"`
	RequestId string             `json:"request_id"`
}

type OrderResponseDTO struct {
	Ordersn     string `json:"ordersn"`
	OrderStatus string `json:"order_status"`
	UpdateTime  int    `json:"update_time"`
}
