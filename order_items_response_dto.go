package shopee_client

type OrderItemsResponseDTO struct {
	Orders    []OrderItemDetailResponseDTO `json:"orders"`
	Errors    []string                     `json:"errors"`
	RequestId string                       `json:"request_id"`
}

type OrderItemDetailResponseDTO struct {
	EstimatedShippingFee string            `json:"estimated_shipping_fee"`
	PaymentMethod        string            `json:"payment_method"`
	UpdateTime           int               `json:"update_time"`
	MessageToSeller      string            `json:"message_to_seller"`
	ShippingCarrier      string            `json:"shipping_carrier"`
	Currency             string            `json:"currency"`
	CreateTime           int               `json:"create_time"`
	PayTime              int               `json:"pay_time"`
	Note                 string            `json:"note"`
	CreditCardNumber     string            `json:"credit_card_number"`
	DaysToShip           int               `json:"days_to_ship"`
	IsSplitUp            bool              `json:"is_split_up"`
	ShipByDate           int               `json:"ship_by_date"`
	EscrowTax            string            `json:"escrow_tax"`
	TrackingNo           string            `json:"tracking_no"`
	OrderStatus          string            `json:"order_status"`
	NoteUpdateTime       int               `json:"note_update_time"`
	FmTn                 string            `json:"fm_tn"`
	DropshipperPhone     string            `json:"dropshipper_phone"`
	CancelReason         string            `json:"cancel_reason"`
	RecipientAddress     RecepitentAddress `json:"recipient_address"`
	CancelBy             string            `json:"cancel_by"`
	EscrowAmount         string            `json:"escrow_amount"`
	BuyerCancelReason    string            `json:"buyer_cancel_reason"`
	GoodsToDeclare       bool              `json:"goods_to_declare"`
	TotalAmount          string            `json:"total_amount"`
	ServiceCode          string            `json:"service_code"`
	Country              string            `json:"country"`
	ActualShippingCost   string            `json:"actual_shipping_cost"`
	Cod                  bool              `json:"cod"`
	Items                []Item            `json:"items"`
	Ordersn              string            `json:"ordersn"`
	Dropshipper          string            `json:"dropshipper"`
	BuyerUsername        string            `json:"buyer_username"`
}

type RecepitentAddress struct {
	Town        string `json:"town"`
	City        string `json:"city"`
	Name        string `json:"name"`
	District    string `json:"district"`
	Country     string `json:"country"`
	Zipcode     string `json:"zipcode"`
	FullAddress string `json:"full_address"`
	Phone       string `json:"phone"`
	State       string `json:"state"`
}

type Item struct {
	Weight                     float32 `json:"weight"`
	ItemName                   string  `json:"item_name"`
	IsWholesale                bool    `json:"is_wholesale"`
	PromotionType              string  `json:"promotion_type"`
	ItemSku                    string  `json:"item_sku"`
	VariationDiscountedPrice   string  `json:"variation_discounted_price"`
	VariationId                int     `json:"variation_id"`
	VariationName              string  `json:"variation_name"`
	IsAddOnDeal                bool    `json:"is_add_on_deal"`
	ItemId                     int     `json:"item_id"`
	PromotionId                int     `json:"promotion_id"`
	AddOnDealId                int     `json:"add_on_deal_id"`
	VariationQuantityPurchased int     `json:"variation_quantity_purchased"`
	VariationSku               string  `json:"variation_sku"`
	VariationOriginalOrice     int     `json:"variation_original_orice"`
	IsMainItem                 bool    `json:"is_main_item"`
}
