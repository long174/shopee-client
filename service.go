package shopee_client

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"net/http"

	"bitbucket.org/snapmartinc/httpclient/middleware"
	log "bitbucket.org/snapmartinc/logger"
)

type Service interface {
	GetToken(redirectURL string, partnerKey string) (result string)
	GetOrders(ctx context.Context, shopperId int, request OrdersRequestDTO) (resp OrdersResponseDTO, err error)
	GetOrderDetails(ctx context.Context, shopperId int, request OrderItemsRequestDTO) (resp OrderItemsResponseDTO, err error)
}

type service struct {
	client *client
}

// Ensure client satisfy service interface
var _ Service = &service{}

func NewService() *service {
	c := GetAppConfigFromEnv()
	httpClient := new(http.Client)

	logger := log.NewLoggerFactory(log.InfoLevel).
		Logger(context.TODO())

	transport := http.DefaultTransport
	transport = middleware.WithMiddleware(
		transport,
		middleware.NewRequestLogger(logger),
		middleware.NewResponseLogger(logger),
	)

	httpClient.Transport = transport

	client := &client{
		config:     c,
		httpClient: httpClient,
	}

	return &service{
		client: client,
	}
}

// Get category tree
func (s *service) GetOrders(
	ctx context.Context,
	shopperId int,
	request OrdersRequestDTO,
) (resp OrdersResponseDTO, err error) {
	err = s.client.DoRequest(ctx, shopperId, "POST", "/api/v1/orders/get", request, &resp)

	return resp, err
}

// Get category tree
func (s *service) GetOrderDetails(
	ctx context.Context,
	shopperId int,
	request OrderItemsRequestDTO,
) (resp OrderItemsResponseDTO, err error) {
	err = s.client.DoRequest(ctx, shopperId, "POST", "/api/v1/orders/detail", request, &resp)

	return resp, err
}

func (s *service) GetToken(redirectURL string, partnerKey string) (result string) {
	baseStr := partnerKey + redirectURL
	h := sha256.New()
	h.Write([]byte(baseStr))
	result = hex.EncodeToString(h.Sum(nil))
	return result
}
