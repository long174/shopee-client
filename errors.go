package shopee_client

import "fmt"

type Error struct {
	Code    int
	Message string
}

func (e *Error) Error() string {
	return fmt.Sprintf("httpclient error code %v, detail: %s", e.Code, e.Message)
}
