module bitbucket.org/snapmartinc/lzd-client

go 1.12

require (
	bitbucket.org/snapmartinc/httpclient v0.0.0-20200206101742-e15af0173d21
	bitbucket.org/snapmartinc/logger v0.0.0-20190722102907-70e1fed01587
	github.com/kelseyhightower/envconfig v1.4.0
)
